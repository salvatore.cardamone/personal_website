AUTHOR = 'Salvatore Cardamone'
SITENAME = 'obstinateotter'
SITEURL = 'https://www.obstinateotter.com'

PATH = 'content'
ARTICLE_PATHS = ['pages', 'blogs']

TIMEZONE = 'GB'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

PLUGIN_PATHS = ['./pelican-plugins']
PLUGINS = ['render_math',]

PROJECTS = [
    {
        'name': 'High-Level Synthesis',
        'url': 'high-level-synthesis.html',
        'description': 'An attempt to make FPGAs more accessible for computational physical sciences'
    },
    {
        'name': 'Digital Signal Processing',
        'url': 'digital-signal-processing.html',
        'description': 'A place to capture basic bits and pieces so I don\'t have to constantly search for explanations'
    },
    {
        'name': 'Software',
        'url': 'software.html',
        'description': 'Some software tricks and "best practices" along the way (disclaimer: may be bad practices)'
    },
]

# Social widget
SOCIAL = (
    ('Gitlab', 'https://gitlab.com/salvatore.cardamone'),
    ('LinkedIn', 'https://www.linkedin.com/in/salvatore-cardamone/'),
    ('Twitter', 'https://www.twitter.com/savcardamone/')
)
SOCIAL_PROFILE_LABEL = u'Get In Touch'

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = "elegant"
LANDING_PAGE_TITLE = "The Obstinate Otter Site"

COMMENTBOX_PROJECT = True
COMMENTBOX_FILTER = True

