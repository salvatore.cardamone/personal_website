author: Salvatore Cardamone
title: Blogs
layout: page
date: 2022-08-19

Here's a list of blogs that are available:

[Vitis HLS for Peripheral Control]({filename}../blogs/high-level-synthesis/peripheral-control/chapter_01.md)
Looks at the use of Vitis HLS to write software for the PYNQ-Z2 which controls
peripherals and launches the classic "blinky" software asynchronously.

[Unit Testing The Singleton Design Pattern]({filename}../blogs/software/singletons.md)
A few solutions for rejigging the singleton design pattern to make it compatible
with dependency injection for unit testing.