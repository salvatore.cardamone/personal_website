author: Salvatore Cardamone
title: About Me
layout: page
date: 2022-08-19
status: hidden
slug: landing-page-about-hidden

My name is Salvatore Cardamone. I'm a chemist-turned-software developer with a
background in high-performance computing (HPC) and signal processing for digital
communications. This site is primarily devoted to documenting any personal
projects that might be worth sharing with others.

You can find a copy of my CV [here]({static}/pdfs/cv.pdf). Alternatively, feel
free to take a look at my ramblings about [Software]({filename}../blogs/software/software.md),
[High-Level Synthesis]({filename}../blogs/high-level-synthesis/high-level-synthesis.md) or
[Digital Signal Processing]({filename}../blogs/digital-signal-processing/digital-signal-processing.md).
