Title: High-Level Synthesis
Authors: Salvatore Cardamone
Date: 2022-08-19
commentbox_filter: off

## General Thoughts and Background

It seems a shame to me that field-programmable gate array (FPGA) manufacturers
(in this case Xilinx and Intel) go to such an enormous effort to design
high-level synthesis (HLS) tools, only for them to never seemingly be used in
anger. Or rather, they don't appear to be used in any production codes that I'm
aware of in scientific computing. The complexity of HLS must be enormous – I'm
certainly no expert in the implementation details, but mapping from C/C++ to
register-transfer level (RTL) is presumably a mind-boggling task. Perhaps I
just haven’t surveyed the literature comprehensively enough, but FPGAs still
seem to remain the purview of firmware engineers designing bespoke hardware
desciption language (HDL) designs, aside from the occasional toy project that
one might encounter when doing a quick Google search.

I did some postdoctoral work in the use of FPGAs in scientific high-performance
computing (HPC) – using HLS tools provided by Maxeler Technologies to deploy
Quantum Monte Carlo (QMC) on FPGAs, with the idea that they may be leveraged in
HPC facilities in the future for more power efficient computing. Naturally, the
project didn’t really come to much – we developed a small toy application that
seemed to perform pretty well, but that was it. And for the most part, the
underwhelming nature of the result was on me – I wasn’t a computer scientist (I
was barely a theoretical chemist), and there was a considerable learning curve
in place before I could do anything useful. Even by the end of the project, I’d
only just scratched the surface. After a number of years in industry working as
a signal processing engineer (primarily in digital communications), I think I
now have a much greater appreciation for what I was doing, and cringe at the
amateur way in which I was doing it. Which isn't to say I'm now an expert --
just marginally less inept. Hence my attempts to atone!

## Peripheral Control with Vitis HLS and PYNQ
[Chapter 1]({filename}peripheral-control/chapter_01.md) Some initial musings on the purpose of
this project.<br />
[Chapter 2]({filename}peripheral-control/chapter_02.md) Setting up your environment.<br />
[Chapter 3]({filename}peripheral-control/chapter_03.md) First steps with Vitis HLS.<br />
[Chapter 4]({filename}peripheral-control/chapter_04.md) Using PYNQ to control peripherals.<br />
[Chapter 5]({filename}peripheral-control/chapter_05.md) Co-processing and asynchronous launch.<br/>

## Homemade HLS
