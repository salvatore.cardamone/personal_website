Title: Chapter 2: Configuring Your Environment
Authors: Salvatore Cardamone
Date: 2022-08-26
commentbox_filter: off

This is the second part of the [Peripheral Control with HLS]({filename}chapter_01.md) series of posts. You can
head to [Chapter 1]({filename}chapter_01.md) to see the justification for this
series, or head on over to [Chapter 3]({filename}chapter_03.md) to start
building something.

## Jargon Buster

Before getting going, it's worth introducing some terminology that we can refer
back to in later chapters since there's plenty of scope to get terribly
confused. First off, the hardware that I'm using is a PYNQ-Z2 board developed
by Xilinx. This board features a bunch of peripherals; LEDs, dipswitches, push
buttons, Pmod ports, Raspberry Pi and Arduino headers, etc. Most interestingly,
it has a Zynq-7000 System-on-Chip (SoC, part XC7Z020-1CLG400C); a chip that has
both a dual-core ARM Cortex A9 processor and FPGA fabric with 85 thousand logic
cells and 630KB of BRAM. In Xilinx parlance, the ARM processor is referred to as
the processing system (PS) and the FPGA fabric as programmable logic (PL).
Interaction between the two is facilitated by an interconnect comprising AXI
channels -- more on this in a later post.

Next up, let's discuss the software we'll be using. We're looking
to use Xilinx's [PYNQ](http://www.pynq.io) project, enabling users to interact
with FPGAs through python and a Jupyter server running on the PS. We'll get onto
the details of this later, but suffice to say that we generate intellectual
property (IP) cores (think of these as black boxes that are deployed on the
FPGA -- hence their being referred to as "IP"; the underlying implementation is
not exposed to the user), and interact with them through an API that Xilinx
provide for us. Developing the IP cores involves Vitis HLS and Vivado. Both are
provided by Xilinx; Vitis HLS is a HLS tool that takes C/C++ code and
synthesises RTL, while Vivado generates FPGA configurations from RTL blocks.

## Xilinx Tools

There isn't a great deal to say here; Xilinx provides a unified installer that
will automate the installation process. However, it might be useful to reflect
my own setup for the sake of aiding reproducibility.

#### Installing Vivado and Vitis HLS

Go ahead and grab the
[Xilix Unified Installer](https://www.xilinx.com/support/download.html). I'm
running version 2022.1, although we're likely not doing anything that's too
version-dependent. You'll be prompted to make an account prior to being
allowed to download anything. Once all of this is done, open up the installer
(make sure you do so with root privileges on Linux) and click your way through.
Select "Vivado" when asked which product we want to install and then
"Vivado ML Standard" (unless you want to pay for the tools). Next you'll be
asked to select precisely what's included in our installation. We want all of
the "Design Tools" (both Vivado and Vitis HLS), and in the "Devices" section
we similarly want everything we're allowed to select, with the exception of
the "Kria SOMs and Starter Kits". Finally, note the greyed-out option at the
bottom about installing cable drivers. You **should** do as the instruction says
and head over to UG973 for instructions on how to do this -- we'll need the
cable drivers so that Vivado is able to recognise our device and program it with
any generated bitstreams from the hardware manager.

You'll likely be horrified at the size of this installation -- in excess of
50GB! But sadly there's no way around this, so set it installing and don't
expect to be able to use the tool for quite some time! In the meantime, you can
move onto the next section.

#### Board Files

When we try to build an IP core, the tools need to know precisely what
resources are available; from peripherals like LEDs to the number of BRAMs and
CLBs that are on the chip. As such, we need to provide some details about the
board and chipset we're working with. To this end, we need to retrieve files
that describe the PYNQ-Z2 in some format that the Xilinx tools understand. The
actual format and contents of these files is uninteresting (at least it is to
me, hence why I won't even pretend to understand), but we still need to point
the tools to the board files on our system.

We can retrieve the PYNQ-Z2 board files from
[here](https://dpoauwgwqsy2x.cloudfront.net/Download/pynq-z2.zip). On my
system, I've stuck them in `/opt/xilinx/boards/`, and directed both
Vivado to the pynq-z2/A.0 directory using the following tcl command:

```
set_param board.repoPaths [list "/opt/xilinx/boards/pynq-z2/A.0/"]
```

in the initialisation script `~/.Xilinx/Vivado/Vivado_init.tcl`. That's it;
Vivado and Vitis HLS will take care of the rest!

## Using PYNQ

The use of PYNQ is introduced in far more detail
[here](https://pynq.readthedocs.io/en/v2.0/index.html); there's really no point
my trying to duplicate the excellent tutorials that are provided, so go ahead
and complete that before proceeding.

## Other Resources

I should point out from the beginning the several excellent online
resources that I've consulted when writing these posts. The contents of these
posts are by no means unique (although I've tried to include some novelty), and
I'd be remiss to not redirect you to the sources I've found particularly
helpful:

1 . The [PYNQ](https://pynq.io) site has a number of excellent posts on how to
get going. If you look in the "Support" section, you'll see some particularly
helpful tutorials by Cathal McCabe.<br />
2. [hackster](https://hackster.io) has a number of very cool projects that
people have made available to follow, albeit don't tend to include much by means
of explanation.<br />
3. The [Vitis](https://docs.xilinx.com/r/en-US/ug1399-vitis-hls/Getting-Started-with-Vitis-HLS)
documentation is excellent, although more as an all-encompassing reference; it's
not meant to be a guide that's read from start-to-finish, but is wonderfully
exhaustive.<br />
4. I'd very much recommend you try the "Getting Started" guide available
[here](https://xilinx.github.io/Vitis-Tutorials/2021-1/build/html/docs/Getting_Started/Vitis_HLS/Getting_Started_Vitis_HLS.html).
There'll be some duplication with what I'll be writing about in following posts,
but it's well worth repetition if you're not familiar with FPGAs.<br />
5. There's an open-source book/ course called
[pp4fpgas](https://pp4fpgas.readthedocs.io/en/latest/) (Parallel Programming for
FPGAs), intended primarily for engineers. It's truly excellent and worth a
perusal at the very least.<br />
6. We'll find in the following posts that AXI is the primary interface between
components on the PL. Consequently, I'd suggest you take a look at a few
very readable resources that Xilinx have on the matter
[here](https://support.xilinx.com/s/article/1053914?language=en_US) and
Chapter 1
[here](https://docs.xilinx.com/v/u/en-US/ug1037-vivado-axi-reference-guide).