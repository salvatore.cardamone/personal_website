Title: Chapter 1: Introduction
Authors: Salvatore Cardamone
Date: 2022-08-19
commentbox_filter: off

I've had a Xilinx PYNQ-Z2 board lying around in its box for goodness knows how 
long that I got from attending a workshop at FPL in 2018. The whole concept of 
PYNQ is really neat, and there are some really cool projects online that 
deploy this little board for things like edge ML and video processing (see 
[hackster](https://hackster.io) for examples). The idea is that Xilinx
provide a bunch of overlays (effectively FPGA images that can be manipulated 
from python). The user then sets some parameterisations and input/ output 
variables and lets the PL loose on the problem. The purpose is clearly to let 
software engineers without any hardware experience leverage FPGAs for common 
compute-intensive applications with as a low a barrier to entry as possible.

Obviously I have an interest in using Xilinx's HLS tools, and I haven't seen 
anyone provide any tutorials on creating an overlay using Vitis HLS. The 
reasons for this are pretty sensible; 

1. The basic tutorials using PYNQ toggle LEDs and interface with 
peripherals (think the classic "blinky" example). This isn't the intended use 
case for HLS; HLS is a black box, where we don't care how or why the 
toolchain does things so long as we get something synthesisable out the other 
end. Interacting with hardware peripherals introduces the added complexity of 
meeting certain timing constraints, and so we do care about what the HLS is 
doing under-the-hood.
2. If you really want to interact with hardware, you're probably looking at 
some specification that tells you what lines to set and when, so this is 
clearly a scenario where a hardware description language is a better fit than 
C/C++.

In summary, it's actually a pretty dumb idea to create an introductory 
tutorial for PYNQ using Vitis HLS. But, variety is the spice of life, and 
someone's got to train ChatGPT, so I'd may as well get to it! Head over to 
[Chapter 2]({filename}chapter_02.md) to get started!