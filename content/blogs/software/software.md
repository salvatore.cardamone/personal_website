Title: Software
Authors: Salvatore Cardamone
Date: 2022-11-16
commentbox_filter: off

A collection of thoughts on software development, primarily in C/C++.

[Singleton Design Pattern]({filename}singletons.md) A suggestion on how to write your singletons so they can be unit-tested.<br />
[Computational Chemistry Configuration]({filename}configuration_reader.md) A generic extensible configuration reader that can be used for a bunch of different applications in computational chemistry.<br />
[Backsubstitution]({filename}backsubstitution.md) An exhaustive analysis of backsubstitution.<br />


